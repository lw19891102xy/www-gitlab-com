---
layout: markdown_page
title: "Product Direction - Service Desk"
description: "Connect users and customers of your product to support"
canonical_path: "/direction/monitor/service_desk/"
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Service Desk

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2023-03-23` |

### Overview

Service desk is a point of contact between users and service providers. It captures the demand for incident resolution and service requests.

We view the service desk as an integral part of GitLab. Developers use GitLab to create, launch, and operate software products. When there is a demand for incident resolution, service requests, or other forms of feedback from users of the product, we want the feedback loop to be efficient and seamless. GitLab service desk is responsible for bringing these requests directly to the developers all within the same platform.

Furthermore, beyond shortening the feedback loop for developers, in alignment with [GitLab's 10-year vision](/company/vision/#vision), we want GitLab to be a general service desk for all users of the platform.  


## Current Offering (January 2023)

The [GitLab Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html#service-desk) allows your organization the opportunity to provide an email address to your customers. These customers can send issues, feature requests, comments, and suggestions via email, with no external tools needed. These emails become issues right inside GitLab, potentially even in the same project where you are developing your product or service, pulling your customers directly into your DevOps process.

### Strategy and Themes
<!-- Capture the main problems to be solved in the market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

#### Themes

Service desk solutions should be able to solve the following problems:

1. **Intake:** Service desk should provide channels for access. Depending on the use case, channels may include phone, service portals, mobile applications, chat, emails, walk-ins, sms, social media, and public and corporate forums. The intake of service requests should assist in capturing the requisite details for resolution.
1. **Workflow management:** Service desk agents, people who respond to service requests, have the following needs:
   - Understand what work should be prioritized
   - Use the appropriate tools to fulfill service requests and/or resolve requests for help
   - Capture and log the appropriate information to coordinate with their team and facilitate organizational learning
1. **Management Reporting:** Service Desk is responsible for capturing the demand for help and service requests. Being primarily a reactive activity, understanding the scope and type of work, is critical to enable proactive planning.
1. **Audit:** What was requested and any subsequent activities and resolution on service requests, are captured and stored. Service desk work items should be auditable.

#### Strategy

GitLab Service Desk is built on top of existing GitLab capabilities, including [work item types](https://docs.gitlab.com/ee/development/work_items.html), [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html), [on-call schedule management](https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html), Analytics Dashboards, and more. We intend to leverage the capabilities built for DevOps use cases and add enhancements to efficiently address the needs of service desk solutions.

We plan to do the following:

1. **Shorten the feedback loop for developers** - GitLab, as a platform primarily used by developers, we want to provide developers, who also support their products directly, to receive service requests of their customers within GitLab. This is what GitLab Service Desk does today. We plan to prioritize capabilities against this use case.
1. **Dogfooding GitLab Service Desk** - GitLab, similar to other enterprise companies, has several [service desk use cases](https://gitlab.com/groups/gitlab-org/-/epics/10137) internally. We want to build against requirements identified internally to replace existing vendor tools with GitLab. We are starting with [IT Ops](https://docs.google.com/presentation/d/1fXjsguUOnYHH0_S_DBNMZtbFlJx3Im_pyl8irthZ1Is/edit#slide=id.g15907daa010_0_0) (GitLab Internal Link).

### 1-year plan
<!--
1-year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road map". Items in this section should be linked to issues or epics that are up to date. Indicate the relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

In 2023, GitLab is renewing the investment in the Service Desk category because we view the adjacent ITSM market as one in which we can be successful by enhancing the capabilities we have built for DevOps.

We plan to do the following in 2023 in priority order

1. Enable GitLab IT Ops to use GitLab Service Desk. Specifically, we are prioritizing Intake via [Slack Workflow](https://gitlab.com/groups/gitlab-org/-/epics/10041), workflow management via [real time boards](https://gitlab.com/gitlab-org/gitlab/-/issues/16020) and [Additional Swimlane options](https://gitlab.com/groups/gitlab-org/-/epics/328), and management reporting.
1. Address long-lived usability issues and key missing features to [shorten the feedback loop for developers](#strategy). We will start by ensuring we have the appropriate product analytics and monitoring of the Service Desk.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics that encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

Between 2023-02-01 and 2023-04-30:

1. Wrap up previous in-flight work on Incident Management
2. Complete [Metrics Deprecation and Removal](https://gitlab.com/groups/gitlab-org/-/epics/10107)
3. Complete [Self-monitoring Deprecation and Removal](https://gitlab.com/groups/gitlab-org/-/epics/10030)
4. Ramp up on introductory [Service Desk issues](https://gitlab.com/gitlab-org/monitor/respond/-/issues/206#note_1303136452)
5. Improve access request work intake via [Slack workflows](https://gitlab.com/groups/gitlab-org/-/epics/10041)

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

Please see our [15.11 plan](https://gitlab.com/gitlab-org/monitor/respond/-/issues/206).

#### What we recently completed

GitLab Service Desk has not been a priority in recent years. We will provide quarterly updates going forward.

#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

TBD

#### Top [1/2/3] Competitive Solutions
<!-- PMs can choose to highlight a primary BIC competitor--or more if no single clear winner in the category exists; in this section, we should indicate: 1. name of a competitive product, 2. links to marketing website and documentation, 3. why we view them as the primary BIC competitor -->

##### [Atlassian Jira Service Management](https://www.atlassian.com/software/jira/service-management)

Atlassian very successfully pivoted Jira project management and issue tracking into Jira Service Management. We view them as the exemplary case of having transitioned from a software planning tool to tackling the much larger market of ITSM. The way Atlassian started was to implement service desk on top of existing Jira capabilities.

##### [Zendesk](https://www.zendesk.com/)

Zendesk is a powerful service desk tool with broad market appeal. It is the feature complete service desk tool that we also use at GitLab for external customer support. 

##### [ServiceNow](https://www.servicenow.com/)

ServiceNow is a dominant player in ITSM and offers a comprehensive enterprise solution with service desk, along with other management and workflow tools built in. 

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in users' goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

1. [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer) - Software developers may be directly responsible to support the software they built, such as for internal tools. This is the persona GitLab Service Desk supports today.
1. Support Engineer - These engineers are the customer-facing representatives of the business and want to be able to efficiently resolve problems as they arise. They are frustrated by manual steps which divert their focus from solving real problems for the customers they serve and strive to represent their company in the best way possible. This is the longer-term target persona for GitLab Service Desk.

### Pricing and Packaging

Service Desk is currently available in the free tier. We will revisit our pricing and packaging strategy for service desk.

### Analyst Landscape

TBD
