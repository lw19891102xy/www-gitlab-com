title: Deutsche Telekom
file_name: deutsche-telekom
canonical_path: /customers/deutsche-telekom
twitter_image: /images/blogimages/deutsche-telekom-twitter-1024x512_norman-stamnitz.png
cover_image: /images/blogimages/dl-5g-02.jpg
cover_title: Deutsche Telekom drives DevSecOps transformation with GitLab Ultimate
cover_description: With GitLab, Deutsche Telekom has created a single source of
  truth for the company’s developers, driving increased productivity, enhanced
  security, and faster time to market.
customer_logo: /images/case_study_logos/dl-telekom-logo-02.jpg
customer_industry: Telecommunications
customer_location: Bonn, Germany
customer_solution: GitLab Ultimate
customer_employees: 216,500 (Dec 31, 2021)
customer_overview: Europe’s leading telco turned to the GitLab DevSecOps
  Platform to foster collaboration and eliminate inefficiencies without
  sacrificing security.
customer_challenge: Europe’s leading telco turned to the GitLab DevSecOps
  Platform to foster collaboration and eliminate inefficiencies without
  sacrificing security.
key_benefits: >-
  **Faster time to market**: GitLab’s DevSecOps Platform has allowed Deutsche
  Telekom to cut down on manual tasks such as build and deployment processes,
  allowing the company to produce more software, faster.


  **Streamlined security**: With GitLab’s built in-security and compliance features, developers at Deutsche Telekom have been able to integrate security and compliance into their daily routine and improve the security of their applications.


  **Improved collaboration**: With GitLab, Deutsche Telekom now has a single platform where developers can share code, facilitating knowledge transfer across departments.
customer_stats:
  - stat: 6x
    label: faster time to market
  - stat: 13,000
    label: active GitLab users
customer_study_content:
  - subtitle: "Deutsche Telekom: The leading telco"
    content: "[Deutsche Telekom
      AG](https://www.telekom.com/en/company/companyprofile/company-profile-625\
      808) is a German telecommunications company and one of the world's leading
      integrated telecommunications companies, serving more than 240 million
      mobile customers, 26 million fixed-network lines, and 22 million broadband
      lines in more than 50 countries. By taking a legacy industry — the classic
      telephone company — and digitizing all aspects of the business, Deutsche
      Telekom represents a new type of service company: a software company that
      sells telecommunications services."
  - subtitle: Improving efficiency and reducing time to market with GitLab Premium
    content: >-
      As Europe’s leading telco, Deutsche Telekom understands the importance of
      DevOps in driving efficiencies in the software development lifecycle. “Of
      course, DevOps is not just the tooling, but also the mindset, the culture,
      the way people work together,” says Thorsten Bastian, business owner of
      the CI/CD Hub of Telekom IT. DevOps methodologies have become a
      cornerstone of Deutsche Telekom’s efforts to streamline software
      development and cut down on manual tasks, break down silos, increase
      collaboration and productivity, and speed up time to market.


      But that didn’t happen overnight. For several years, as Deutsche Telekom transitioned from a waterfall approach to Agile methodology, different software development teams within the company began to consider how to leverage automation and continuous integration and delivery (CI/CD), but adoption was spotty in the beginning. Because teams used different tools for automation, a single source of truth for sharing or collaborating on code was not available.


      Telekom IT, a division of Deutsche Telekom that designs, develops, and runs IT systems for the company, saw the need for a centralized platform where developers could share code and draw from a common set of functionalities for automation and CI/CD. “We needed to reduce manual tasks so that people can really focus on more complex activities in innovative parts of the whole development process,” says Bastian. 


      Norman Stamnitz, product manager of Telekom IT’s [CI/CD toolsuite](https://devops.telekom.de/en) — which is built on top of GitLab — explains that a user-driven selection process ultimately brought them to GitLab. “As part of the whole DevOps and Agile approach, we didn’t want to decide this from the top down,” says Stamnitz. “We really wanted the people who would be using the platform to decide what makes sense for them. That’s how we came to GitLab.” Stamnitz and team started with GitLab’s Premium tier, as they wanted access to enterprise-level features such as priority support. 


      Telekom IT made it a priority to ensure that all developers or DevOps engineers within Deutsche Telekom could use GitLab. The CI/CD toolsuite needed to be accessible on any kind of laptop, without the need to register for a separate account or fill out a complicated order form. “After the system was available, we only did a little advertising in internal communities, and after that it ran by itself,” says Stamnitz. “In a very short time we had over 1,000 users on the platform — and that was without any requirements from IT governance or the like. Our CI/CD toolsuite with GitLab at its core spread like wildfire via word of mouth.”


      And it wasn't just projects and users from Telekom IT that moved over to GitLab. Other IT units across the company also decided to switch off their own CI/CD systems (some of them already using GitLab, some using other tools) and migrate to Telekom IT’s central GitLab Premium instance.


      Now, two and a half years later, Telekom IT has more than 13,000 active users from all over the company in GitLab, and roughly 75% of the company’s Agile programs are using Telekom IT’s CI/CD toolsuite. Feedback from users has been overwhelmingly positive, Stamnitz says. “They're always very thankful that we offer the platform, and that they don't have to maintain it themselves — that it's just there, and it’s just working. The experience for the developers, I think, is quite good.”


      Part of this enhanced developer experience is a shift towards “inner source” — a culture of sharing code and knowledge inside the organization. “Before purchasing GitLab Premium, it was difficult to find a way to facilitate code sharing across different departments within the company. Of course we had various code repositories, like Git or Subversion, but code sharing was always a problem,” says Stamnitz. “People would say, ‘I’m sure this has already been developed hundreds of times, but I can’t access the source code.’ That has changed with our central GitLab installation, because now, we are all hosting our source code, more or less, on the same platform. Everybody can see it and participate.”
  - subtitle: Shifting security left with GitLab Ultimate
    content: >-
      Two years after rolling out GitLab Premium, Telekom IT began to examine
      parts of its software development lifecycle where manual tasks and
      bottlenecks remained. What stood out most was security. 


      “We decided to extend to GitLab Ultimate because we wanted to have the security and compliance features and all in one security dashboard,” says Stamnitz. “If you can reduce manual security processes, do all this security scanning before a go-live — that brings us the ability to speed up or to reduce the time to market even more. And of course, we wanted to shift left. We wanted our developers to have security scanners as part of their daily tasks.”


      “Regarding the security functionality, of course, it’s a huge benefit,” says Bastian. “If you have it integrated in one application, you can immediately jump to the right place and fix the problem, instead of sending reports to the projects about the findings. This is increasing the efficiency of handling security findings.”
  - subtitle: Partnering with GitLab
    content: >-
      In GitLab, Telekom IT has found a trusted technology partner, and Telekom
      IT plans to make GitLab the standard software development platform across
      the company. As one aspect of this long-term strategic partnership,
      Telekom IT has become a key contributor to the GitLab platform and works
      closely with GitLab product teams on feature requests and open beta
      programs.


      “We’ve submitted several feature requests to ask for new features and made some small contributions to GitLab on our own,” says Stamnitz. “In general, that's going very smoothly. Things that we contributed have been included in the very next release, and overall the GitLab team helps us get things solved really quickly.”


      Developers at Deutsche Telekom also appreciate GitLab’s high-velocity release schedule, with releases happening on the 22nd of every month. Telekom IT usually installs GitLab updates within one or two days of the release due to high demand from across the company. “People see that new GitLab features are out and they immediately ask us when we’re installing it,” says Stamnitz.


      A more collaborative culture is empowering Deutsche Telekom’s software development teams to produce more software faster, with the same number of people. “Before, some people were using GitLab, but others were using GitHub Actions, and still others were using Jenkins or other tools. Everybody was using their own thing,” Stamnitz adds. “Now, everybody's using the same platform. I would say that we are better in what we are doing now than we were before. And quicker.”
customer_study_quotes:
  - blockquote: Time to market was a big issue for us. Before our transformation to
      Agile and DevOps started, we had release cycles of nearly 18 months in
      some cases. We’ve been able to dramatically reduce that to roughly 3
      months.
    attribution: Thorsten Bastian
    attribution_title: Business Owner IT, CI/CD Hub, Telekom IT
